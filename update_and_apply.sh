#!/usr/bin/env bash
set -eux

cd /etc/puppet/code/
./update.sh
./apply.sh
