### studio computer av latency:

![studio computer av latency](./diagram/studio_computer_av_latency.svg)

This diagram shows what causes latency and where it is compensated.
#### 1: plugin latency
When plugins inside ardour use lookahead or upsampling, they need extra time.

#### 2: insert latency
When all programs are connected in series, jack processes them all within the soundcard latency.
When a the output of a program goes trough another program and then back into the first program, so in other words: when a program has an insert, we get another period of latency.
The encoder, although drawn as a separate block, is part of obs, so we have this second kind of connection in our case.

#### 3: soundcard latency
The regular latency caused by the buffers and the hardware.

### keeping audio and video in sync (and latency correction correct)

Video uses the system clock as its source of time, audio uses the clock of the soundcard, this means that over time those clocks will drift and things will be out of sync (and buffers start filling up, things being dropped, etc.)

#### Option 1: master clock (keeps everything in sync so it doesn't drift over time)

There is a thing called iee1588, and the intel i210 card has support for it, including interfacing to the analog world (it can receive 1pps signals from for. example a gps, and if i read correctly it can generate 48khz output signals (that probably can clock the soundcard). Also there exist software to sync the system clock with high precision to those signals on the network card, as a nice extra it will allow you to use avb gear (for ex. motu ultralite avb), and other pro audio network protocols.

* [[https://github.com/AaronWebster/igb-pps-guide]]
* [[https://tsn.readthedocs.io/]]

![master clock](./diagram/master_clock.svg)

#### Option 2: use cpu clock as audio clock 

This might work when using the dummy backend of jack, and resample (using j2a / a2j) when playing or recording through a soundcard.

* [[https://jack-devel.jackaudio.narkive.com/JSaKufWJ/jackd-dummy-driver-clock]]


