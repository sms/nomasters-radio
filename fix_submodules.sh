#!/usr/bin/env bash
set -eux
git pull
git config --global status.submoduleSummary true



#git submodule sync: Updates the description of submodules cached by git in .git/modules from the just-edited source of truth specified in .gitmodules.
git submodule sync

#Updates the checked-out submodules in the working copy. (--remote means doing git pull in the submodule)
git submodule update --init --recursive --remote

# check out the configured branch in .gitmodules, if there is none, checkout master
git submodule foreach -q --recursive 'git checkout $(git config -f $toplevel/.gitmodules submodule.$name.branch || echo master)'

# update branch to latest
git submodule foreach 'git pull origin $(git config -f $toplevel/.gitmodules submodule.$name.branch || echo master)'

# hold back some modules that are broken in master

cd modules/concat
git checkout b19758fbbf78fa9ea4abee49948914fbbf04a40a
cd ../../

cd modules/tor
git checkout 7d901a491e28ac2de9b6eeb08390a9b9c346eb72
cd ../../

cd modules/stdlib
git checkout v8.3.0
cd ../../

cd modules/firewall
git checkout v5.0.0
cd ../../

cd modules/apt
git checkout v8.5.0
cd ../../

cd modules/docker
git checkout v7.0.0
cd ../../

cd modules/influxdb
git checkout v2.3.2
cd ../../

cd modules/telegraf
git checkout v4.3.1
cd ../../


#probably only needed for debian buster
cd modules/vcsrepo
git checkout v5.5.0
cd ../../
