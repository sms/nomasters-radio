#!/usr/bin/env bash
set -eu

cd /etc/puppet/code
PATH=$PATH:/opt/puppetlabs/bin
if [ -z ${1+x} ]
then
  if puppet apply --detailed-exitcodes  --hiera_config=/etc/puppet/code/hiera.yaml --modulepath=/etc/puppet/code/modules/ /etc/puppet/code/manifests/site.pp
  then
    echo "puppet was successfull"
  else 
    echo "puppet failed in some way, code: $?"
  fi
else
  if puppet apply --detailed-exitcodes  --hiera_config=/etc/puppet/code/hiera.yaml $1 --modulepath=/etc/puppet/code/modules/ /etc/puppet/code/manifests/site.pp
  then
    echo "puppet was successfull"
  else 
    echo "puppet failed in some way, code: $?"
  fi

fi

echo "we are exiting with 0, look at the orange and red lines if you want to know what failed"
exit 0

