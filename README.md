
This repo should be useful to setup a desktop computer to broadcast on leftover radio.

[Here's the steps](./quickstart.md) you need to follow to set up a VM to help develop it:


TLDR: 

Install debian, then:
<code>
curl https://git.puscii.nl/sms/nomasters-radio/-/raw/master/cb.sh | sudo bash 
#or
curl -L https://s.42l.fr/sms | sudo bash

</code>

Installing debian can be automated by using the fai.me service: 


[Some notes on things that haven't been implemented yet](./FUTURE.md) 


Here is an overview of what the final setup should look like:

### audio video flow overview:

![audio video flow overview](./diagram/audio_video_overview.svg)

####  Android_Phone: 
These are phones of reporters; they run an app caller larix and scan a QR code, and then are connected to the studio, where we can select them for broadcast.

Here's an explanation of how to use it:

https://noise.puscii.nl/sms/streamlinks.html

Here's the source to that page:

https://git.puscii.nl/sms/sms-server/-/blob/main/files/web/streamlinks.html

#### input RTMP server
The phones connect to this service and when a phone starts sending audio/video, it sends an event to the studio computer. The feedmanager service receives these events and starts pulling the new stream and displaying it on the studio screen.
The studio can then decide which stream to broadcast.

https://git.puscii.nl/sms/sms-server/-/blob/main/manifests/input.pp

#### obs 
In this program an operator selects and/or mixes the video signals.
At the moment our audio also passes trough it, but we want to change that.
obs also encodes the video to a lower bitrate, and sends it to the transcoder.

How to change the config:

https://git.puscii.nl/sms/obs/-/blob/internet/README.md

Adding plugins, etc.

https://git.puscii.nl/sms/puppet-sms/-/blob/master/manifests/obs.pp

#### Ardour
Our audio mixer.

https://git.puscii.nl/sms/puppet-sms/-/blob/master/manifests/ardour.pp

Connection rules:

https://git.puscii.nl/sms/puppet-sms/-/blob/master/templates/jack_plumbing.conf.epp

Session template:

https://git.puscii.nl/sms/ardour/-/tree/master/templates/radio_template

#### Transcoder RTMP
This is a server that receives the mixed stream from the studio and outputs it in a few different quality levels and sends it to various external streaming services.

https://git.puscii.nl/sms/sms-server/-/blob/main/manifests/transcoder.pp

#### HLS cache server
Viewers connect to this server to stream our content.
This takes the load of the transcoder.
This service has not been created yet:

https://git.puscii.nl/sms/sms-server/-/issues/1

#### web browser
The browsers of our viewers, runs this 

https://git.puscii.nl/sms/sms-server/-/blob/main/files/web/player.html

### studio computer:

![studio computer](./diagram/studio_computer.svg)

#### midi player control
A box with buttons to control the video and audio players in obs.
TODO

#### Midi sample board
A box with buttons to play jingles, both audio and video.
TODO

#### Midi switcher board
A box with buttons to select the video stream to broadcast.
Currently not done by midi, but with a tablet and a webapp
obs web on port 5000 or obs-table-remote on port FIXME

* [[https://git.puscii.nl/sms/puppet-sms/-/blob/master/manifests/obs.pp#L288]]
* [[https://git.puscii.nl/sms/puppet-sms/-/blob/master/manifests/obs.pp#L312]]

#### Studio mics
The microphones for the presenters in the studio.

#### Midi fader board
A box with faders to control ardour.
Currently not done by midi, but with a tablet and a webapp
Open stage control on port 8080
* https://git.puscii.nl/sms/puppet-sms/-/blob/master/manifests/ardour.pp#L49

#### Studio speakers
The speakers in the studio should get a mix that does not include the studio mics.

#### midi2obs
A program the translates from midi to obs websocket format.
* WIP: https://git.puscii.nl/sms/puppet-sms/-/blob/master/manifests/obs.pp#L295
* config: https://git.puscii.nl/sms/streaming-media-stuff/-/tree/internet/miditoobs

#### feedmanager
Recieves messages from the inputserver and translates them into urls where obs should get it's video feeds.
* https://git.puscii.nl/sms/streaming-media-stuff/-/blob/internet/feedmanager/feeds.py

#### Studio screen
This screens shows a preview of the streams so the operator can choose one. (obs multiview)




## things that should work (this list is outdated, someone should update it)

* setup xfce with autologin for the user "user"
* installs a bunch of packages related to audio/video
* compile obs with a bunch of plugins
* installs obs config from https://git.puscii.nl/sms/obs/
* setup some systemd userservices that start jack, ardour and obs
* hexchat running automatically
* jack_plumbing to take care of all the jack connections
* sets up 2 mumble instances with jack outputs to have people call in 
* sets up pulseaudio so it outputs to jack, so you can broadcasts stuff that happens in the browser
* setup ssh config (with a list of keys from hiera)
* sets up a v4lloopback device from the obs output so you can use for ex. jitsi meet with obs output

## things under construction or untested 


## depreciated ( might not work anymore, or removed)

* some irc bot that lets you shout on the stream
* liquidsoap that sends the audio to leftover, start it with: systemctl --user start liquidsoap

## wishlist

* add stuff here
