#!/usr/bin/env bash


cl=DEBIAN,DHCPC,DEMO,FAIBASE,BOOKWORM,ONE,SSH_SERVER,STANDARD,NONFREE,REBOOT,FAIME,GRUB_PC,GRUB_EFI,AMD64
fai-mirror  -C /etc/fai-bookworm -m1 -c$cl /tmp/mirror
fai-cd -C /etc/fai-bookworm -g grub.cfg.install-only -m/tmp/mirror faime-7OPMZABL.iso
