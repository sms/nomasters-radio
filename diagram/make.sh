#!/usr/bin/env bash

function makepng () {
  dot -T png $1.dot > $1.png
  dot -T svg $1.dot > $1.svg


}

makepng studio_computer_av_latency
makepng studio_computer
makepng audio_video_overview
makepng master_clock
