FROM debian:bullseye

RUN apt update
RUN apt install -y puppet screen vim git
ADD . /etc/puppet/code

WORKDIR /etc/puppet/code/
#RUN puppet module list --tree
#RUN puppet lookup --compile smsserver::record_path
#RUN puppet apply --modulepath=/etc/puppet/code/modules/ -e "lookup smsserver::record_path"
RUN ls  /etc/puppet/code
RUN puppet module list --tree
RUN puppet apply  --hiera_config=/etc/puppet/code/hiera.yaml --modulepath=/etc/puppet/modules:/etc/puppet/code/modules/ -e "include sms::ardour"

CMD bash
