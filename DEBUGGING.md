

tricks: 

- only let puppet do the stuff you work on:
  ./apply.sh "--tags smsserver"
  
  Tags are assigned automatic, you can use module names or class names.

environment variables in /etc/default/sms:

Set G_MESSAGES_DEBUG=all to enable WirePlumber’s debug output.
Set PIPEWIRE_DEBUG=n (n=1-5) to enable PipeWire’s debug output.

