#!/usr/bin/env bash
set -eux

echo "updating and applying manifests, when you have unsaved changes, this might / will not work (i.e do not use this script when making changes to parent repo)"

cd /etc/puppet/code

mkdir -p /etc/puppet/private/nodes/
touch /etc/puppet/private/nodes/`hostname`.yaml
touch /etc/puppet/private/common.yaml


git pull

git submodule sync --recursive
git submodule update --init --recursive
