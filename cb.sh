#!/usr/bin/env bash
#
set -eux

echo "installing radio puppets"

apt update
apt -y install puppet screen vim git lsb-release
git clone --recurse-submodules https://git.puscii.nl/sms/nomasters-radio.git /etc/puppet/code
cd /etc/puppet/code
git checkout master
./update_and_apply.sh

