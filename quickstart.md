# Quickstart (how to setup a development machine)


## 1. cloning this repo on your own machine
On your own machine (assuming you have write access to this repo)
<code>
git clone --recursive git@185.52.224.4:sms/nomasters-radio.git
</code>


####  Before you start working in the repo: 
<code>
./fix_submodules.sh  # this will update all our dependencies (in submodules) to their latest version, so that might introduce changes, if that is the case, just commit them cause we probably want to have the last version. If you know git and submodules, there is no need to use the fix_submodules script.
</code>

#### editing files in submodules
<code>
to edit the sms puppet scripts that contain for example the audio scripts:
cd nomasters-radio/modules/sms
git checkout master # make sure you are on the correct branch
git pull origin master # this makes sure you have the latest work of others (otherwise you will have to deal with nasty merge conflicts)
vim manifests/audio.pp 
git commit manifests/audio.pp
git push --set-upstream origin master #  this will publish your work in the puppet-sms repo
cd ../../ # go back to nomasters-radio
git commit modules/sms# to commit the changes to the nomasters repo, check what you commit and make sure you know what you are doing
git push # this will publish the nomaster-radio repo, so that it refers to the latest version of the sms module
</code>

## 2. create a development machine

### vagrant
1.  install vagrant and virtualbox
2. vagrant up --provision
### manually: 
Create a virtualbox vm (a physical machine is fine too), 25G disk, Intel HD Audio, enable audio input, disable 3D acceleration, enable shared clipboard

Install debian 11 on it with the installer, select ssh server and the xfce desktop environment.

Create a user called "user".

Make sure you give it a unique hostname, we will need it later.

If you made a vm install guest additions: https://kifarunix.com/install-virtualbox-guest-additions-on-debian-11/


## 3. create a config file for your development machine

Add a file with the hostname of the computer you are going to install this on in [this_repo]/hiera/nodes/[hostname].yaml

Put in there all the non secret config (look at hiera/nodes/example.yaml for examples).

If you have those secret values, create /etc/puppet/private/common.yaml on your development machine, with this contents:

<code>
---
sms::gitrepos::localchanges: true
sms::glwritetoken: askforthesecretthatcomeshere
sms::webdav_url: https://thecloudurl
sms::webdav_user: webdav_username
sms::webdav_password: webdav_password

</code>

Commit and push the repo.



## 4. apply the config you just created on your development machine

Login on the desktop as the user "user" (otherwise certain things are broken), open a terminal, become root.

Make sure you run this in screen (or set your terminal emulator's scrollback to infinite), or have some other way of capturing the output of the commands.

When you get errors, report them as bugs on https://git.puscii.nl/sms/nomasters-radio/-/issues

Also make sure you capture the output of the first run, and report it here: https://git.puscii.nl/sms/nomasters-radio/-/issues

<code>
apt update
apt install puppet screen vim git lsb-release
git clone --recurse-submodules https://git.puscii.nl/sms/nomasters-radio.git /etc/puppet/code
cd /etc/puppet/code
git checkout master
./update_and_apply.sh
</code>


To get the latest version after that:

<code>
cd /etc/puppet/code
./update_and_apply.sh
</code>




